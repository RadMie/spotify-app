/** @ngInject */
class SearchController {
  constructor(SpotifyService, BookmarksService, StorageService) {
    this.SpotifyService = SpotifyService;
    this.BookmarksService = BookmarksService;
    this.StorageService = StorageService;
    this.albums = [];
    this.search = this.StorageService.get('search') || '';
    this.searchAlbums(this.search);
  }

  searchAlbums(query) {
    this.StorageService.set('search', query);
    this.SpotifyService.searchAlbums(query)
      .then(response => {
        this.albums = this.filter(response);
      })
      .catch(() => {
        this.albums = [];
      });
  }

  filter(albums) {
    const bookmarks = this.BookmarksService.get();
    albums.forEach(album => {
      if (this.BookmarksService.has(bookmarks, album.id)) {
        album.isAdded = true;
      }
    });
    return albums;
  }

  addBookmark(album) {
    if (this.BookmarksService.add(album)) {
      album.isAdded = true;
    }
  }

}

export default SearchController;
