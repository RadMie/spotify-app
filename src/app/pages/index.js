import angular from 'angular';

import Bookmarks from './bookmarks';
import Search from './search';

const pages = angular.module('SpotifyApp.pages', [
  Bookmarks,
  Search
])
  .name;

export default pages;
