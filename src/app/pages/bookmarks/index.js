import angular from 'angular';

import BookmarksController from './bookmarks.controller';
import BookmarksTemplate from './bookmarks.template.html';

const bookmarks = angular.module('SpotifyApp.pages.bookmarks', [])
  .controller('BookmarksController', BookmarksController)
  .config(config)
  .name;

/** @ngInject */
function config($stateProvider) {
  $stateProvider
    .state('bookmarks', {
      url: '/bookmarks',
      template: BookmarksTemplate,
      controller: 'BookmarksController',
      controllerAs: 'ctrl'
    });
}

export default bookmarks;
