/** @ngInject */
class BookmarksController {
  constructor(StorageService) {
    this.StorageService = StorageService;
    this.bookmarks = this.getBookmarks();
  }

  getBookmarks() {
    return this.StorageService.get('bookmarks') || [];
  }

  remove(id) {
    this.bookmarks = this.bookmarks.filter(item => item.id !== id);
    this.StorageService.set('bookmarks', this.bookmarks);
  }
}

export default BookmarksController;
