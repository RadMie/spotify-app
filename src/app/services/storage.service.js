import angular from 'angular';

/** @ngInject */
class StorageService {

  constructor($window) {
    this.localStorage = $window.localStorage;
  }

  get(key) {
    const data = localStorage.getItem(key);
    if (!data) {
      return null;
    }
    return angular.fromJson(data);
  }

  set(key, data) {
    this.localStorage.setItem(key, angular.toJson(data));
  }

  clear(key) {
    this.localStorage.removeItem(key);
  }
}

export default StorageService;
