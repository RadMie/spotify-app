/** @ngInject */
class SpotifyService {

  constructor($http) {
    this.$http = $http;
  }

  searchAlbums(query) {
    const apiUrl = `https://api.spotify.com/v1/search?type=album&q=${query}`;
    return this.$http.get(apiUrl)
      .then(response => response.data.albums.items);
  }
}

export default SpotifyService;
