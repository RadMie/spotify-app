import angular from 'angular';

import SpotifyService from './spotify.service';
import StorageService from './storage.service';
import BookmarksService from './bookmarks.service';

const services = angular.module('SpotifyApp.services', [])
  .service('SpotifyService', SpotifyService)
  .service('StorageService', StorageService)
  .service('BookmarksService', BookmarksService)
  .name;

export default services;
