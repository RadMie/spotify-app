/** @ngInject */
class BookmarksService {

  constructor(StorageService) {
    this.StorageService = StorageService;
    this.key = 'bookmarks';
  }

  get() {
    return this.StorageService.get(this.key) || [];
  }

  add(album) {
    const bookmarks = this.get();

    if (!this.has(bookmarks, album.id)) {
      bookmarks.push(album);
      this.StorageService.set(this.key, bookmarks);
      return true;
    }
    return false;
  }

  has(bookmarks, id) {
    return bookmarks.some(bookmark => bookmark.id === id);
  }

}

export default BookmarksService;
