/** @ngInject */
class NavbarController {
  constructor() {
    this.items = [
      {state: 'search', label: 'Search', icon: 'search'},
      {state: 'bookmarks', label: 'Bookmarks', icon: 'bookmark_border'}
    ];
  }
}

export default NavbarController;
