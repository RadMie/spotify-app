import template from './navbar.template.html';
import controller from './navbar.controller';

/** @ngInject */
const NavbarComponent = {
  template,
  controller
};

export default NavbarComponent;
