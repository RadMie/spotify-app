import angular from 'angular';

import NavbarComponents from './navbar/navbar.component';

const components = angular.module('SpotifyApp.component', [])
  .component('navbar', NavbarComponents)
  .name;

export default components;
