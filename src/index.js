import angular from 'angular';
import uiRouter from 'angular-ui-router';
import ngAnimate from 'angular-animate';
import './index.scss';
import './assets/waves';

import Pages from './app/pages';
import Components from './app/components';
import Services from './app/services';

const root = angular.module('SpotifyApp', [
  uiRouter,
  ngAnimate,
  Pages,
  Components,
  Services
])
  .config(config)
  .name;

/** @ngInject */
function config($locationProvider, $urlRouterProvider) {
  $locationProvider.html5Mode(true).hashPrefix('!');
  $urlRouterProvider.otherwise('/search');
}

export default root;
